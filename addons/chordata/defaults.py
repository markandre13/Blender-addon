# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import datetime
from mathutils import Color
from enum import IntFlag, auto

ADDON_VERSION = (0, 0, 0) #!! Warning is overwritten by the script `set_version.sh` which runs in CI

ADDON_VER_STR = "{}.{}.{}".format(*ADDON_VERSION)

GIT_HASH = '#dev' #!! Warning is overwritten by the script `set_version.sh` which runs in CI

# ================================
# =           MESSAGES           =
# ================================

ENGINE_REPORTS = {
    "started": "Chordata Engine (v{}, {}) started".format(ADDON_VER_STR, GIT_HASH),
    
    "stopped": "Chordata Engine stopped",

    "already_running": "Chordata Engine already running",

    "node_tree_not_found" : """The node_tree [{}] could not be found. The Chordata Engine was not stopped. 
                You might need to restart Blender to keep working. 
                See the console for more details, and please report this error""",

    "calib_error": "An error was found during the pose-calibration process\n{}",
    
    "deps_error": """Error installing python dependencies for the Chordata add-on.
    It might be due to a temporary connectivity issue.
    Please check your connection and try again.
    If the problem persists please report the error on the Chordata forum pasting the output from the system console
    Error: {}""",
   
    "key_error": "The Chordata engine could not find a previusly valid object. Perhaps it was renamed while the engine was running\n{}",
    
    "unknown_error": "Stopping Chordata engine due to an unknown error\n{}\nStack trace:\n{}",
    
    "network_error": "Stopping Chordata engine due to a networking error\n{}"
}

# ======  End of MESSAGES  =======

# ======================================
# =           KEYS, GUI, ETC           =
# ======================================

ENGINE_CANCEL_KEYS = {'F9'}

NODES_REDRAW_RATE = 0.1 #in seconds


# ======  End of KEYS, GUI, ETC  =======

# ==============================
# =           COLORS           =
# ==============================

color_red = (1.0, 0.4, 0.216, 1)

color_vio = (0.23, 0.4, 0.8, 1)

color_grey = (0, 0.87, 0.7, 1)

color_ocre_grey = (130/255, 130/255, 89/255, 1)
color_light_teal = (89/255, 130/255, 109/255, 1)

bone_no_info = Color((0.3,0.3,0.3))
bone_ok = Color((0.15,0.2,0.15))

# ======  End of COLORS  =======


# =================================
# =           Templates           =
# =================================
from os.path import join, abspath, dirname

TEMPLATES_DIR = join(dirname(abspath(__file__)), "templates")
TEMPLATES_FILE = join(TEMPLATES_DIR, "chordata_templates_v120.blend")
CUSTOM_FILE = join(dirname(abspath(__file__)), "nodes/custom.py")
DEF_BIPED_FILE = join(TEMPLATES_DIR, "arm_default_biped.xml")
WORKSPACE_NAME = "Mocap"
NODETREE_NAME = "Chordata Demo Conf"
ARM_NODETREE_NAMES = ("Chordata Full Biped 17", "Chordata Full Biped 15")
CUBE_NODETREE_NAME = "Chordata Test Cube"
ARMATURE_NAME = "Chordata_biped"
AVATAR_MODEL_NAME = "Chordata_Avatar_v1"
KC_MODEL_NAME = "KCeptor_R1"
KCPP_MODEL_NAME = "Kceptor++"
FAKE_BONES_COLLECTION  = "Objects as bones"
DEMO_ANIM_SUFX = "ChordataDemo_"
POSE_SPACE_G_SUFX = "(pose space) "


# ======  End of Templates  =======

# ============================
# =           MISC           =
# ============================

logger_name = "Chordata_addon_logger"

TIMEKEEPER_HIST_LEN = 10

CALIB_QUEUE_SIZE = 24

CALIBRATION_STATES = [
    ("NO-CALIB", "No calibrated", "This bone doesn't have calibration data"),
    ("OUTDATED", "Calibration outdated", "This bone have calibration data, but it's older than the rest of the armature"),
    ("CALIBRATED", "Calibrated", "This bone has valid calibration data"),
    ("INVALID", "Invalid", "An error ocurred while creating the calibration data"), 
]

CALIBRATION_DEF_STATE = "NO-CALIB" 


class Calibration_Phase(IntFlag):
    NONE            = 0
    # LEGACY = auto()
    VERT            = 1<<0
    ARMS            = 1<<2
    LEFT_LEG        = 1<<3
    RIGHT_LEG       = 1<<4
    TRUNK           = 1<<5
    
    DONE_VERT       = 1<<6
    DONE_ARMS       = 1<<7
    DONE_LEFT_LEG   = 1<<8
    DONE_RIGHT_LEG  = 1<<9
    DONE_TRUNK      = 1<<10

DOING_CALIB_MASK = \
    Calibration_Phase.VERT      |\
    Calibration_Phase.ARMS      |\
    Calibration_Phase.LEFT_LEG  |\
    Calibration_Phase.RIGHT_LEG |\
    Calibration_Phase.TRUNK
DONE_CALIB_MASK = \
    Calibration_Phase.DONE_VERT      |\
    Calibration_Phase.DONE_ARMS      |\
    Calibration_Phase.DONE_LEFT_LEG  |\
    Calibration_Phase.DONE_RIGHT_LEG |\
    Calibration_Phase.DONE_TRUNK


# ======  End of MISC  =======

# ==============================
# =           ERRORS           =
# ==============================
class Chordata_error(Exception):
	pass

class Calibration_error(Chordata_error):
	pass

class Armature_error(Chordata_error):
	pass

class Network_error(Chordata_error):
	pass


# ======  End of ERRORS  =======


# ===============================================
# =           Notochord configuration           =
# ===============================================

TEXTBLOCK_XML_NAME = "last_chordata_configs"

XML_COMMENT = "Generated by Chordata's Blender addon v{} @ {}".format( ADDON_VER_STR,
    datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

configurations_dict = {
    'Configuration': {
        'tag': "Configuration",
        'KC_revision': {
            'tag': "KC_revision",
            'text': "2"
        },
        'Communication': {
            'tag': "Communication",
            'Adapter': {
                'tag': "Adapter",
                'text': "/dev/i2c-1"
            },
            'Ip': {
                'tag': "Ip",
                'text': "127.0.0.1"
            },
            'Port': {
                'tag': "Port",
                'text': "6565"
            },
            'Log': {
                'tag': "Log",
                'text': "stdout,file"
            },
            'Transmit': {
                'tag': "Transmit",
                'text': "osc"
            },
            'Send_rate': {
                'tag': "Send_rate",
                'text': "50"
            },
            'Verbosity': {
                'tag': "Verbosity",
                'text': "0"
            }
        },
        'Osc': {
            'tag': "Osc",
            'Base': {
                'tag': "Base",
                'text': "/Chordata"
            }
        },
        'Fusion': {
            'tag': "Fusion",
            'Beta_start': {
                'tag': "Beta_start",
                'text': "1.0"
            },
            'Beta_end': {
                'tag': "Beta_end",
                'text': "0.2"
            },
            'Time': {
                'tag': "Time",
                'text': "5000"
            }
        }
    }
}

# ======  End of Notochord configuration  =======
