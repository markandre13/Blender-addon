try:
	import requests
except ModuleNotFoundError:
    requests = None
     
from ctypes import c_char_p

from threading import Thread

from . import gui

class IP:
	value = None
 
def _async_request(requests_dict, request_id, method, url, kwargs={}):
	kwargs["stream"] = True
	if not "timeout" in kwargs:
		kwargs["timeout"] = 2
	try:
		response = requests.request(method, url, **kwargs)
	except Exception as e:
		print("Error on async request:", e)
		if request_id in requests_dict:
			requests_dict[request_id] = (0, None, method, url)
		return
	IP.value = response.raw._original_response.fp.raw._sock.getsockname()[0]
	if not request_id in requests_dict:
		return
	requests_dict[request_id] = (response.status_code, response.text, method, url)

class ConnectionChecker:
	_possible_domains = [
		"notochord",
		"notochord.wlan",
		"notochord.local",
		"192.168.85.1"
	]
	_possible_ports = [
		80, 5000, 5001
	]

	_possible_http_urls = None

	_requests = {}
	_request_id = 0
	
	_request_urls = {}

	_found_http_url = None

	@classmethod
	def _build_possible_http_urls(cls):
		if cls._possible_http_urls is not None:
			return
		cls._possible_http_urls = [
			v[i] for v in map(
				lambda addr: [
					f"http://{addr}:{port}" for port in cls._possible_ports
				], cls._possible_domains
			) for i in range(len(cls._possible_ports))
		]

	@classmethod
	def _remove_request(cls, request_id):
		if not request_id in cls._requests:
			return
		del cls._requests[request_id]
		del cls._request_urls[request_id]

	@classmethod
	def _send_request(cls, url):
		request_id = cls._request_id
		cls._request_id += 1

		cls._requests[request_id] = None
		t = Thread(
			daemon=True,
			name="AsyncHTTP",
			target=_async_request,
			args=(
				cls._requests, request_id,
				"GET", url
			)
		)
		t.start()

		return request_id

	@classmethod
	def check(cls, http_url=None):
		if http_url is not None:
			requests_keys = list(cls._requests.keys())
			if len(requests_keys) == 0:
				request_id = cls._send_request(f"{http_url}/state?clear_registry=true")
				cls._request_urls[request_id] = http_url

				gui.console_write(f"Checking connection at {http_url}")
				return None

			request_id = requests_keys[0]
			request = cls._requests[request_id]
			if request is None:
				return None

			if request[0] > 0:
				gui.console_write("Connection found")
				cls._found_http_url = cls._request_urls[request_id]
				return True
			return False

		if cls._found_http_url is not None:
			return True

		requests_keys = list(cls._requests.keys())
		if len(requests_keys) > 0:
			request_id = requests_keys[0]
			request = cls._requests[request_id]
			if request is None:
				return None
			
			found_http_url = cls._request_urls[request_id]
			cls._remove_request(request_id)
			if request[0] > 0:
				gui.console_write("Connection found")
				cls._found_http_url = found_http_url
				return True

		cls._build_possible_http_urls()
		if len(cls._possible_http_urls) == 0:
			gui.console_write("Connection not found")
			return False

		url = cls._possible_http_urls.pop(0)

		request_id = cls._send_request(f"{url}/state?clear_registry=true")
		cls._request_urls[request_id] = url

		gui.console_write(f"Checking connection at {url}")
		return None

	@classmethod
	def get_http_url(cls):
		return cls._found_http_url
	
	@classmethod
	def reset(cls):
		for key in list(cls._requests.keys()):
			del cls._requests[key]
		for key in list(cls._request_urls.keys()):
			del cls._request_urls[key]
		cls._possible_http_urls = None

class AsyncHTTP:
	_id = 0
	_requests = {}

	@staticmethod
	def check_connection(host=None, port=None):
		return ConnectionChecker.check(
			None if host is None else f"http://{host}:{port}"
		)
	
	@staticmethod
	def get_http_url():
		return ConnectionChecker.get_http_url()
	
	@staticmethod
	def reset_connection_checker():
		return ConnectionChecker.reset()

	@classmethod
	def request(cls, method, url, **kwargs):
		request_id = cls._id
		cls._id += 1
		addr = ConnectionChecker.get_http_url()
		if addr is None:
			url = ".none./{}".format(url if not url.startswith("/") else url[1:])
			cls._requests[request_id] = (0, None, method, url)
		else:
			cls._requests[request_id] = None
			t = Thread(
				daemon=True,
				name="AsyncHTTP",
				target=_async_request,
				args=(
					cls._requests,
					request_id,
					method,
					"{}/{}".format(
						addr, url if not url.startswith("/") else url[1:]
					),
					kwargs
				)
			)
			t.start()
		return request_id

	@classmethod
	def get_ip(cls):
		return IP.value

	@classmethod
	def get_request(cls, request_id):
		if not request_id in cls._requests:
			return None
		return cls._requests[request_id]

	@classmethod
	def remove_request(cls, request_id):
		if not request_id in cls._requests:
			return
		del cls._requests[request_id]