# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
from ..utils import out, average_quaternions
from .. import defaults
from collections import deque
import numpy as np
from datetime import datetime
from mathutils import Color
from os import path

try:
	from .calibration_manager import Calibration_Manager
except ModuleNotFoundError:
	from ..defaults import Calibration_error
	class Calibration_Manager:
		def __init__(self):
			raise Calibration_error("Dependencies not found")

averageQuaternions = average_quaternions.averageQuaternions



def find_root_bone(bones):  
	root = None
	for b in bones:
		if not b.parent: #pragma: no branch
			if root: #pragma: no branch
				raise defaults.Armature_error("more than one Root bone found.")

			root = b

	if root: #pragma: no branch
		return root

	raise defaults.Armature_error("No Root bone found.")


def find_lowest_bone(bones): #pragma: no-cover
	lowest = 0
	for b in bones:
		lowest = min(lowest, b.tail.z)
	return lowest

def quat_queue_to_Nx4matrix(quats):
	pre_matrix = [tuple(q) for q in quats]
	return np.array(pre_matrix)


class Armature_Manager:

	@property
	def id_armature(self):
		if not self._arm_ref:
			self._arm_ref = bpy.data.objects[self._object]
		return self._arm_ref

	@property
	def pbones(self):
		if not self._pose_bones_ref:
			self._pose_bones_ref = bpy.data.objects[self._object].pose.bones
		return self._pose_bones_ref
	
	@property
	def dbones(self):
		if not self._data_bones_ref:
			self._data_bones_ref = bpy.data.objects[self._object].data.bones
		return self._data_bones_ref

	@property
	def root_dbone(self):
		if not hasattr(self, "_rbone"): #pragma: no branch
			self._rbone = find_root_bone(self.dbones).name
		return bpy.data.objects[self._object].data.bones[ self._rbone ]

	@property
	def root_pbone(self):
		if not hasattr(self, "_rbone"): #pragma: no branch
			self._rbone = find_root_bone(self.dbones).name
		if not self._root_bone_ref:
			self._root_bone_ref = bpy.data.objects[self._object].pose.bones[ self._rbone ]
		return self._root_bone_ref

	def update_ID_data_refs(self):
		"""Calling this function before each round will remove old references to
		Blender's ID datablocks, forcing the Armature_manager to get new ones.
		This avoids crashing due to dangling pointer references"""
		self._arm_ref = None
		self._pose_bones_ref = None
		self._data_bones_ref = None
		self._root_bone_ref = None

	def __init__(self, settings):
		self.arm_settings = settings
		armature_object = settings.armature_ob
		calib_queue_size = settings.calib_queue_size
		if not armature_object or armature_object.type != "ARMATURE":
			raise defaults.Armature_error("Armature_Manager: The armature object is not valid")

		self._object = armature_object.name
		out.info("Chordata Armature_Manager init with ['{}'] armature object".format(self._object))

		self.update_ID_data_refs()

		# Keep a copy of the root's local matrix
		# This is the transformation in 'EDIT_MODE' of the bone 
		# inside the local (object) reference frame
		self.root_local_matrix = self.root_dbone.matrix_local.copy()
		self.root_pbone.chordata.Q_local_instant_conj = self.root_local_matrix.to_quaternion().conjugated()
		out.debug("Armature's root matrix:\n{}".format(self.root_local_matrix))

		self.prepare_armature()

		self.reset_pose()
		
		# Find the lowest starting bone, and the relative height of the root at startup
		self.lowest_z = find_lowest_bone(self.pbones)
		self.rel_root_height = self.root_local_matrix.to_translation().z - self.lowest_z
		self.starting_ob_height = self.id_armature.location.z
		out.debug("Armature's root relative height: {}".format(self.rel_root_height))


		# Private calibration variables
		self._calib_queue_size = calib_queue_size
		self._calibrating = False
		self._calib_values = {}

		self.context = None
  
		self.calib_manager = Calibration_Manager()

		#end __init__

	def set_context(self, context):
		self.context = context

	# ====================================
	# =		   QUAT HANDLING		   =
	# ====================================
	
	def receive_quat(self, bone_name, quat):
		try:
			self.pbones[bone_name].chordata.Q_temp_received = quat
			self.pbones[bone_name].chordata.dirty = True
		except KeyError as e:
			out.debug("Received subtarget [{}] not found in armature")


	def set_calib_timestamp(self):
		now = datetime.now()
		self.id_armature.chordata.last_calibration_timestamp = \
														now.timestamp()
		self.id_armature.chordata.last_calibration_date = \
										now.strftime("%d/%m/%Y, %H:%M:%S")
 
	def process_pose(self, smooth = 0, calibrating = False, calc_root_z = True, canonized=True):
		# if getattr(self, "calib_manager", None):
		# 	self.calib_manager.store_packet(packet)
  
		if self.arm_settings.legacy_calib and calibrating: #legacy calibration
			if not self._calibrating:
				# -- START_CALIBRATION --
				self.reset_pose()
				self._calibrating = True

			# -- DOING_CALIBRATION --
			self.store_calibration_values()
			
		else:
			if self._calibrating:
				self._calibrating = False
				self.legacy_calib()

				self.set_calib_timestamp()

				# -- END_CALIBRATION --

			self.process_bone(self.root_pbone, smooth, canonized)

		lowest = 10000
		if calc_root_z:
			for b in self.pbones:
				lowest = min(b.tail.z, lowest)

			self.id_armature.location.z = self.starting_ob_height - lowest 

	def process_bone(self, bone, smooth, canonized):
		if bone.chordata.dirty:
			if self.arm_settings.legacy_calib:
				#apply legacy calib
  				new_quat = bone.chordata.Q_local_instant_conj \
					@ bone.chordata.Q_temp_received \
					@ bone.chordata.Q_calibration
			else:
				#apply new calib
				new_quat = bone.chordata.Q_local_instant_conj \
					@ bone.chordata.Q_pre_calib \
	 				@ bone.chordata.Q_temp_received \
					@ bone.chordata.Q_post_calib
	   

			#Keep the a "canonized" form of quat to avoid interpolation issues
			if canonized and bone.rotation_quaternion.dot(new_quat) < 0:
				new_quat.negate()

			if smooth < 0.01:
				bone.rotation_quaternion = new_quat
			else:
				bone.rotation_quaternion = new_quat.slerp(bone.rotation_quaternion, smooth)

			bone.chordata.dirty = False
			bone.chordata.output_dirty = True

		Qpar_conjugated = bone.matrix.to_quaternion().conjugated()
		for child in bone.children:
			child.chordata.Q_local_instant_conj = child.chordata.Q_local_conjugated @ Qpar_conjugated
			self.process_bone(child, smooth, canonized)

	
	def store_calibration_values(self):
		for bone in self.pbones:
			if bone.chordata.dirty:
				if bone.name not in self._calib_values:
				 	self._calib_values[bone.name] = deque( maxlen = self._calib_queue_size )

				self._calib_values[bone.name].append(bone.chordata.Q_temp_received.copy())

	def store_packet(self, packet, calib_settings, set_phase):
		if set_phase:
			self.calib_manager.set_phase(calib_settings)
		
		self.calib_manager.store_packet(packet, force=calib_settings.store_extra)
		
		if calib_settings.dump_extra:
			now_str = self.calib_manager.out["time"]
			filepath = path.join(path.dirname(bpy.data.filepath), 
						"{}-Chordata_calib_dump_extra.csv".format(now_str))
			self.calib_manager.dump_entries_DF(filepath)
			calib_settings.dump_extra = False
			self.calib_manager.free()

	def calibrate(self, calib_settings):
		pre_calib, post_calib, now_str = self.calib_manager.run(calib_settings)
		for bone in self.pbones:
			try:
				bone.chordata.Q_pre_calib = pre_calib[bone.name]
				bone.chordata.Q_post_calib = post_calib[bone.name]
			except KeyError:
				pass
		
		self.set_calib_timestamp()
  
		if self.arm_settings.calib.dump_calib_data and bpy.data.is_saved:
			filepath = path.join(path.dirname(bpy.data.filepath), 
							"{}-Chordata_calib_dump.csv".format(now_str))
			self.calib_manager.dump_entries_DF(filepath)
   
			filepath = path.join(path.dirname(bpy.data.filepath), 
						"{}-Chordata_calib_data.json".format(now_str))
			self.calib_manager.dump_calib_data(filepath)

		self.calib_manager.free()

	def sta_correction(self,calib_settings):
		bone_l = 'l-upperarm'
		bone_r = 'r-upperarm'
  
		print("Applying STA {}:{:.4f} | {}:{:.4f}".format(bone_l,calib_settings.sta_fac_l,
												bone_r,calib_settings.sta_fac_r))
  
		res = self.calib_manager.run_sta_correction(calib_settings.sta_fac_l, 
												  		calib_settings.sta_fac_r,
												  		bone_l, bone_r)

		if not res:
			return

		q_l, q_r = res

		print("Pre_calib L: {} | before:{}".format( q_l, self.pbones[bone_l].chordata.Q_pre_calib))
		print("Pre_calib R: {} | before:{}".format( q_r, self.pbones[bone_r].chordata.Q_pre_calib))
		
		self.pbones[bone_l].chordata.Q_pre_calib = q_l
		self.pbones[bone_r].chordata.Q_pre_calib = q_r



	def legacy_calib(self):
		for bone in self.pbones:
			try:
				if len(self._calib_values[bone.name]) < self._calib_queue_size:
					raise ValueError()

				q_matrix = quat_queue_to_Nx4matrix(self._calib_values[bone.name])
				avg_q = averageQuaternions(q_matrix)
				bone.chordata.Q_avg_incoming = avg_q

				Qbone_w = bone.matrix.to_quaternion()
				bone.chordata.Q_calibration = Qbone_w.rotation_difference( avg_q ).conjugated()

				bone.chordata.calibration_state = "CALIBRATED"
				
			except (KeyError, ValueError):
				if bone.chordata.calibration_state == "NO-CALIB":
					bone.chordata.calibration_state = "INVALID"
					bone.chordata.Q_calibration.identity()

				elif bone.chordata.calibration_state == "CALIBRATED":
					bone.chordata.calibration_state = "OUTDATED"

		self._calib_values = {}
		

	
	# ======  End of QUAT HANDLING  =======

	# ==============================
	# =           EXTRAS           =
	# ==============================
	def set_color_on_bone(self, bone_name, mcov, _range):
		try:
			self.pbones[bone_name].chordata.mag_covariance += mcov
			self.pbones[bone_name].chordata.mag_covariance /= 2
			bone_group = self.id_armature.pose.bone_groups[bone_name]
			bone_group.colors.normal = defaults.bone_ok.copy()
			val = np.interp(self.pbones[bone_name].chordata.mag_covariance,
				_range,
				(defaults.bone_ok.r,1))
			bone_group.colors.normal.r = val
			bone_group.colors.normal.g = 1-val 

		except KeyError as e:
			out.debug("Received subtarget [{}] not found in armature")
	
	# ======  End of EXTRAS  =======
	
	
	# =============================================
	# =		   ARMATURE HOUSEKEEPING		   =
	# =============================================
	def remove_bone_groups(self):
		for g in self.id_armature.pose.bone_groups:
			self.id_armature.pose.bone_groups.remove(g)

	def set_mode(self, mode):
		if type(self.context) != bpy.types.Context:
			raise TypeError("Cannot change armature mode, incorrect context.\nContext type:{}".format(type(context)))

		self.context.view_layer.objects.active = self.id_armature

		if not mode:
			mode = self.initial_mode

		if bpy.ops.object.mode_set.poll():
			bpy.ops.object.mode_set(mode=mode)

	def prepare_armature(self):
		self.id_armature.location = (0,0,0)
		self.remove_bone_groups()
		self.initial_mode = self.id_armature.mode[:] 

		found_bones = "Bones found on armature:\n"
		for b in self.pbones:
			b.chordata.Q_local_conjugated = self.dbones[b.name].matrix.to_quaternion().conjugated()

			if not b.chordata.capture_bone:
				found_bones += "{:_>30} NO CAPTURE\n".format(b.name)
				continue
			
			found_bones += "{:_>30}\n".format(b.name)
			self.dbones[b.name].use_inherit_rotation = True

			#Add a boneGroup to every bone
			#it will be used to color the bone according
			#to the magnetic distortion
			bg = self.id_armature.pose.bone_groups.new(name=b.name)
			b.bone_group = bg
			bg.color_set = 'CUSTOM'
			color = defaults.bone_no_info.copy()
			bg.colors.normal = color
			bg.colors.select = color
			bg.colors.active = color

		out.debug(found_bones)


	def reset_pose(self):
		for b in self.pbones:
			b.rotation_quaternion.identity()
	
	# ======  End of ARMATURE HOUSEKEEPING  =======


# ==================================================
# =		   ARMATURE / BONE PROPERTIES		   =
# ==================================================

class Chordata_Bone_Properties(bpy.types.PropertyGroup):
	capture_bone: bpy.props.BoolProperty(name = "Capture Bone", 
		description = "Chordata will use this bone to place motion capture data", default = True)
	
	transmit_bone: bpy.props.BoolProperty(name = "Transmit Bone", 
		description = "Output information of this bone throught OSC", default = False)

	dirty: bpy.props.BoolProperty(name = "Dirty flag", 
		description = "Used by the Chordata engine to mark this bone need to be processed", default = False)

	output_dirty: bpy.props.BoolProperty(name = "Dirty flag for output", 
		description = "Used by the Chordata engine to mark this bone need to be transmited or recorded", default = False)

	Q_temp_received: bpy.props.FloatVectorProperty(name="Temporary received quat", 
		description="The Chordata engine uses this temporary prop to place the received quaternion data before processing it.", 
		default=(1, 0, 0, 0), size=4, subtype='QUATERNION')

	Q_local_instant_conj: bpy.props.FloatVectorProperty(name="Inverted instant local rotation", 
		description="The inverted local rotation of this bone with parent rotation calculated by the Chordata engine at each tick", 
		default=(1, 0, 0, 0), size=4, subtype='QUATERNION')

	Q_local_conjugated: bpy.props.FloatVectorProperty(name="Inverted local rotation", 
		description="The inverted local rotation of this bone at rest", default=(1, 0, 0, 0),
		size=4, subtype='QUATERNION')
		
	Q_avg_incoming: bpy.props.FloatVectorProperty(name="Averaged incoming rotations", 
		description="Averaged incoming rotations", default=(1, 0, 0, 0),
		size=4, subtype='QUATERNION')

	Q_calibration: bpy.props.FloatVectorProperty(name="Calibration rotation (legacy)", 
		description="The pose-calibration rotation. It is the inverted rotation difference between this bone orientation and the corresponding averaged incoming rotations.", default=(1, 0, 0, 0),
		size=4, subtype='QUATERNION')


	Q_pre_calib: bpy.props.FloatVectorProperty(name="Calibration pre-rotation", 
		description="The pose-calibration pre rotation", default=(1, 0, 0, 0),
		size=4, subtype='QUATERNION')

	Q_post_calib: bpy.props.FloatVectorProperty(name="Calibration post-rotation", 
		description="The pose-calibration post rotation", default=(1, 0, 0, 0),
		size=4, subtype='QUATERNION')


	calibration_state: bpy.props.EnumProperty(name="Calibration state (legacy)", 
		description="The pose-calibration state of this bones", 
		items=defaults.CALIBRATION_STATES, default=defaults.CALIBRATION_DEF_STATE )

	mag_covariance: bpy.props.FloatProperty(name = "Bone magnetic covariance", 
		 default = 0)

class Chordata_Arm_Properties(bpy.types.PropertyGroup):
	last_calibration_timestamp: bpy.props.FloatProperty(
		name="Last Calibration Timestamp", default=-1)

	last_calibration_date: bpy.props.StringProperty(
		name="Last Calibration date", default='(no calib data)')

def add_props_to_ArmIDdata():
	bpy.types.PoseBone.chordata = bpy.props.PointerProperty(name="Chordata bone properties",
							description="Chordata bone properties",
							type=Chordata_Bone_Properties)

	bpy.types.PoseBone.rotation_q_pose_space= bpy.props.FloatVectorProperty(name="Rotation in pose space", 
		description="The rotation in pose space. Used to store capture in pose coordinates", default=(1, 0, 0, 0),
		size=4, subtype='QUATERNION')

	bpy.types.Object.chordata = bpy.props.PointerProperty(name="Chordata armature properties",
							description="Chordata armature properties",
							type=Chordata_Arm_Properties)


# ======  End of ARMATURE / BONE PROPERTIES  =======



class Clear_Armature_Calib(bpy.types.Operator):
	"""Reset the calibration data on an armature"""
	bl_idname = "chordata.armature_clear_calib"
	bl_label = "Clear armature calibration"

	armature: bpy.props.StringProperty()

	def execute(self, context):
		try:
			bpy.data.objects[self.armature].chordata.last_calibration_timestamp = -1.0
			bpy.data.objects[self.armature].chordata.last_calibration_date = '(no calib data)'

		except KeyError:
			self.report({'WARNING'}, "No object {} found".format(self.armature))
			return {'CANCELLED'}


		try:
			for bone in bpy.data.objects[self.armature].pose.bones:
				# bone.chordata.Q_calibration = bone.matrix.to_quaternion().conjugated()
				bone.chordata.Q_calibration.identity()
				bone.chordata.Q_pre_calib.identity()
				bone.chordata.Q_post_calib.identity()

		except Exception as e:
			self.report({'ERROR'}, "Error while clearing calibration data in {}.\n{}".format(self.armature, e))
			return {'CANCELLED'}


		return {'FINISHED'}


class Timed_Armature_Calib(bpy.types.Operator):
	"""Reset the calibration data on an armature"""
	bl_idname = "chordata.armature_timed_calib"
	bl_label = "Timed armature calibration"

	delay: bpy.props.IntProperty(default=3)
	calib_time: bpy.props.IntProperty(default=3)
	from_node: bpy.props.StringProperty()
	from_tree: bpy.props.StringProperty()

	def modal(self, context, event):
		if event.type in {'RIGHTMOUSE', 'ESC'}:
			self.cancel(context)
			return {'CANCELLED'}

		if event.type == 'TIMER':
			self.counter += self.delta_t
			tree = bpy.data.node_groups[self.from_tree]
			node = tree.nodes[self.from_node]

			if self.counter < self.delay:
				node.settings.timed_calibration_secs = self.counter
				return {'PASS_THROUGH'}

			node.settings.doing_calibration = True
			node.settings.timed_calibration_secs = self.counter - self.delay

			if self.counter >= self.delay + self.calib_time:
				self.cancel(context)
				return {'FINISHED'}

		return {'PASS_THROUGH'}

	def execute(self, context):
		self.counter = 0
		self.delta_t = 1/context.scene.render.fps
		wm = context.window_manager
		wm.modal_handler_add(self)

		tree = bpy.data.node_groups[self.from_tree]
		node = tree.nodes[self.from_node]
		node.settings.doing_timed_calibration = True

		return {'RUNNING_MODAL'}

	def cancel(self, context):
		wm = context.window_manager
		# wm.event_timer_remove(self._timer)
		tree = bpy.data.node_groups[self.from_tree]
		node = tree.nodes[self.from_node]
		node.settings.doing_timed_calibration = False
		node.settings.doing_calibration = False

to_register = (Chordata_Bone_Properties, Chordata_Arm_Properties, 
	Clear_Armature_Calib, Timed_Armature_Calib)



# ===========================================
# =		   bone_matrix_snippet		   =
# ===========================================
#  import bpy

# D = bpy.data
# C = bpy.context

# def the_quat():
#	 return D.objects['the_helper'].rotation_quaternion


# arm = D.objects['Chordata_biped']

# for b in arm.data.bones:
#	 b.use_inherit_rotation = True


# #set base
# base_local_rotation = arm.data.bones['base'].matrix_local.to_quaternion()
# arm.pose.bones['base'].rotation_quaternion = base_local_rotation.conjugated() @ the_quat()

# #set head
# parent_matrix = arm.pose.bones['head'].parent.matrix

# arm.pose.bones['head'].rotation_quaternion = parent_matrix.to_quaternion().conjugated() @ the_quat()


# 
# 
# ====================================================
# =		   dummy_data_generator snippet		   =
# ====================================================

# import bpy

# C = bpy.context
# the_bone = 'l-arm'
# cone = bpy.data.objects['Cone']
# arm = bpy.data.objects['Armature']
# arm_target = bpy.data.objects['Target']


# Qw = arm.pose.bones[the_bone].matrix.to_quaternion()
# Qpar = arm_target_inh.pose.bones[the_bone].parent.matrix.to_quaternion()
# Qml = arm_target_inh.data.bones[the_bone].matrix_local.to_quaternion()
# Qob = arm_target_inh.rotation_quaternion
# Qroot_local = arm_target_inh.data.bones['l-forarm'].matrix_local.to_quaternion()

# cone.rotation_quaternion =  Qob #object 

# cone.rotation_quaternion @= Qroot_local #root local

# Qroot_pose = arm_target_inh.pose.bones['l-forarm'].rotation_quaternion

# cone.rotation_quaternion @= Qroot_pose #root pose 

# Qbone_local = arm_target_inh.data.bones[the_bone].matrix.to_quaternion()

# cone.rotation_quaternion = Qbone_local #bone local

# arm_target_inh.data.bones[the_bone].use_inherit_rotation = True
# arm_target.pose.bones[the_bone].rotation_quaternion =  Qbone_local.conjugated() @ Qpar.conjugated() @ Qw

