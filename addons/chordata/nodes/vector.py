# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

import bpy
import bmesh
from mathutils import Quaternion, Vector
from .basenode import ChordataBaseNode, set_dirty_flag
from ..ops import engine
from ..utils import gui

import random

def create_subtargets_enum(self, context):
	items = [("no-target",) * 3]

	for subtarget in self.id_data.subtargets:
		items.append((subtarget.name, subtarget.name, subtarget.name))
	return items

raw_vec_items = [
    ("GYRO", "Gyroscope", "Gyroscope data coming on COPP RAW/REAL message", 0),
    ("ACCELEROMETER", "Accelerometer", "Accelerometer data coming on COPP RAW/REAL message", 3),
    ("MAGNETOMETER", "Magnetometer", "Magnetometer data coming on COPP RAW/REAL message", 6),
    ("CUSTOM", "Custom", "Custom offset on the incoming data", 9),
]

raw_vec_dict = {
	"GYRO":0, "ACCELEROMETER":3, "MAGNETOMETER":6, "CUSTOM":0
}

class VectorSettings(bpy.types.PropertyGroup):
	# dump_to_console: bpy.props.BoolProperty(
	# 	name="Dump to system console", default=False, update=set_dirty_flag)

	draw_point_cloud: bpy.props.BoolProperty(
		name="Draw point cloud", default=False)

	remove_helpers_on_stop: bpy.props.BoolProperty(
		name="Remove helpers on stop", default=True, description="Remove helper objects as arrows and point cloud")

	target_object: bpy.props.PointerProperty(
		type=bpy.types.Object, name="Target Object", )

	selected_subtarget: bpy.props.EnumProperty(options={'SKIP_SAVE'},
										  items=create_subtargets_enum, name="Subtarget", default=None, update=set_dirty_flag)

	vec_from_raw: bpy.props.EnumProperty(
				  items=raw_vec_items, name="Display vector property", default='MAGNETOMETER')

	custom_vec_offset: bpy.props.IntProperty(min=0, max=9)

	offset_in_bounds: bpy.props.BoolProperty(default=True)

	has_arrow: bpy.props.BoolProperty(default=False, options={'SKIP_SAVE'})

class VectorNode(ChordataBaseNode):
	'''Visualize a 3D vector as an arrow or pointcloud'''
	bl_idname = 'VectorNodeType'
	bl_label = "Vector Node"

	# === Property Group Pointer ===================================================
	settings: bpy.props.PointerProperty(
		type=VectorSettings)
	# ==============================================================================

	def init(self, context):
		self.width = 320.0
		self.inputs.new('DataStreamSocketType', "data_in")

	def draw_buttons(self, context, layout):
		if not self.settings.offset_in_bounds:
			layout.alert = True
			layout.label(text="Offset {} out of bounds of the incoming data".format(self.settings.custom_vec_offset))

		layout.prop(self.settings,
					"target_object", text="Object")

		layout.prop(self.settings,
					"selected_subtarget", text="Sub-target")

		layout.prop(self.settings,
					"vec_from_raw", text="Vector to display", expand=True)

		if self.settings.vec_from_raw == "CUSTOM":
			layout.prop(self.settings,
					"custom_vec_offset", text="Custom vector offset")


		layout.prop(self.settings,
					"draw_point_cloud", text="Draw point cloud")

		layout.prop(self.settings,
					"remove_helpers_on_stop", text="Remove helper objects on stop")

	def draw_label(self):
		return "Vector visualization Node"

	@engine.helper_method
	def create_point_cloud(self):
		if self.id_settings.draw_point_cloud:
			if not self.point_cloud:
				self.bm = bmesh.new()
				mesh = bpy.data.meshes.new("_{}_point_cloud_mesh".format(self.name))
				point_cloud = bpy.data.objects.new( "_tmp_{}_point_cloud".format(self.name), mesh )
				point_cloud.parent = self.id_settings.target_object
				bpy.context.collection.objects.link(point_cloud)
				self.mesh_name = mesh.name
				self.point_cloud = point_cloud.name

	"""@engine.helper_method
	def add_subtarget(self, subt):
		if subt in self.id_tree.subtargets:
			return

		added = self.id_tree.subtargets.add()
		added.name = subt
	"""

	@engine.helper_method
	def update_point_cloud(self, vec):
		if self.id_settings.draw_point_cloud:
			self.create_point_cloud()
			self.bm.verts.new(vec)
			self.bm.to_mesh(bpy.data.meshes[self.mesh_name])
	
	def on_engine_node_init(self):
		if not hasattr(self, "arrow"):
			self.arrow = None

		if self.id_settings.target_object and not self.id_settings.has_arrow:
			self.arrow = bpy.data.objects.new( "_tmp_{}_arrow".format(self.name), None )
			bpy.context.collection.objects.link(self.arrow)
			self.arrow.parent = self.id_settings.target_object
			self.arrow.empty_display_type = 'SINGLE_ARROW'
			self.arrow.is_chordata_item = True
			self.arrow.rotation_mode = 'QUATERNION'
			self.arrow = self.arrow.name

			self.point_cloud = None
			self.bm = None
			self.create_point_cloud()
			self.id_settings.has_arrow = True

	def on_engine_node_stop(self):
		if self.id_settings.remove_helpers_on_stop:
			if getattr(self, "arrow", None):
				bpy.data.objects.remove(bpy.data.objects[self.arrow])
			if getattr(self, "point_cloud", None):
				bpy.data.objects.remove(bpy.data.objects[self.point_cloud])
				bpy.data.meshes.remove(bpy.data.meshes[self.mesh_name])
		
		if getattr(self, "bm", None):
			self.bm.free()

		self.id_settings.has_arrow = False


	@engine.datatarget_handler(engine.DataTarget.RAW)
	def visualize_vector(self, packet):
		for msg in packet.get():
			if self.arrow: #pragma: no branch
				if self.id_settings.selected_subtarget == msg.subtarget:
					if self.id_settings.vec_from_raw == "CUSTOM":
						offset = self.id_settings.custom_vec_offset
					else:
						offset = raw_vec_dict[self.id_settings.vec_from_raw]
						self.id_settings.custom_vec_offset = offset

					if offset + 2 >= len(msg.payload):
						self.id_settings.offset_in_bounds = False
						return
					else:
						self.id_settings.offset_in_bounds = True

					v = Vector(( msg.payload[offset], msg.payload[offset+1], msg.payload[offset+2]))

					v /= 1000
					self.update_point_cloud(v) 
					arrow = bpy.data.objects[self.arrow]
					arrow.empty_display_size = v.magnitude 
					v.normalize()
					arrow.rotation_quaternion = v.to_track_quat('Z','Y')

		packet.restore()


	@engine.datatarget_handler(engine.DataTarget.EXTRA)
	def calibration_pointcloud(self, packet):
		if not hasattr(packet, "time"):
			return

		if not hasattr(self, "last_pointcloud_time") or self.last_pointcloud_time != packet.time:
			self.last_pointcloud_time = packet.time
			self.pointcloud_msgs = 0
			if self.bm:
				self.bm.free()
			self.bm = bmesh.new()

		print( "t:{} | msgs {:4d}: ".format(self.last_pointcloud_time, self.pointcloud_msgs) )

		for msg in packet.get():
			self.pointcloud_msgs += 1
			v = Vector(( msg.payload[0], msg.payload[1], msg.payload[2]))
			v /= 1000
			self.update_point_cloud(v) 

		#this is just to flag the object for re-evaluation 
		bpy.data.objects[self.point_cloud].location.z = random.random() * 0.001


to_register = (VectorSettings, VectorNode)

