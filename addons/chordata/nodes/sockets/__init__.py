# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

if "bpy" in locals():
    import imp
    imp.reload(datastreamsocket)
    imp.reload(statssocket)
    imp.reload(armaturesocket)
    imp.reload(hierarchysocket)
else:
    from . import datastreamsocket
    from . import statssocket
    from . import armaturesocket
    from . import hierarchysocket

import bpy

to_register = datastreamsocket.to_register \
			+ statssocket.to_register \
            + armaturesocket.to_register \
            + hierarchysocket.to_register             


def register():
    from bpy.utils import register_class
    for cls in to_register:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(to_register):
        unregister_class(cls)
