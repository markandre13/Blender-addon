from bpy.types import NodeSocket
from ... import defaults

class HierarchySocket(NodeSocket):
    '''Socket defining KCeptor hierarchy'''
    bl_idname = 'HierarchySocketType'
    bl_label = "Hierarchy Node Socket"

    def draw(self, context, layout, node, text):
        layout.label(text="Hierarchy")

    def draw_color(self, context, node):
        return defaults.color_light_teal

class KCeptorSocket(NodeSocket):
    '''Socket connecting KCeptors'''
    bl_idname = 'KCeptorSocketType'
    bl_label = 'KCeptor Node Socket'

    def draw(self, context, layout, node, text):
        layout.label(text=text)

    def draw_color(self, context, node):
        return defaults.color_ocre_grey

to_register = (HierarchySocket, KCeptorSocket)
