# Chordata client (Blender addon)  
# -- Motion capture manager for the Chordata Open Source motion capture system
#
# http://chordata.cc
# contact@chordata.cc
#
#
# Copyright 2018-2019 Bruno Laurencich
# Copyright 2020-2022 Bruno Laurencich, Lorenzo Micozzi Ferri
#
# This file is part of Chordata client (Blender addon).
#
# Chordata client (Blender addon) is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Chordata client is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Chordata client (Blender addon).  
# If not, see <https://www.gnu.org/licenses/>.
#
#
# -- END OF COPYRIGHT & LICENSE NOTICES -- 

from .. import defaults
from ..chordatatree import ChordataTreeNode
from .basenode import verify_links_valid
from bpy.types import Node, NodeSocket, PropertyGroup

class StatsNode(Node, ChordataTreeNode):
	bl_idname = 'StatsNodeType'
	bl_label = "Stats Node"

	def init(self, context):
		self.width = 300.0
		self.inputs.new('StatsSocketType', "stats")

	def update(self):
		verify_links_valid(self)

	def draw_buttons(self, context, layout):
		if self.inputs['stats'].is_linked:
			stats_output = self.inputs['stats'].links[0].from_socket
			# rate = from_node.stats.get_rate()
			if stats_output.config.packet_rate != -1:
				packet_rate =  "Packet rate: {:4.2f}".format(stats_output.config.packet_rate)
			else:
				packet_rate = "Packet rate: (no packets received)"
			layout.label(text = packet_rate )
			layout.label(text = "Packets received:  {:6d}".format(stats_output.config.packets_n) )
			layout.label(text = "Messages received: {:6d}".format(stats_output.config.msgs_n) )
			

		else:
			layout.label(text = "Conect a node to get it's stats")


to_register = (StatsNode, )