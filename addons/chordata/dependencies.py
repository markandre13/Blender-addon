import bpy
import sys
import importlib as imp
from .defaults import ENGINE_REPORTS, Chordata_error
from .utils import out
from traceback import format_exc
from os import path

target_dir = path.realpath(path.join(path.dirname(__file__), "../modules"))

def check_import():
    deps_missing = False 
    deps_missing = deps_missing or (False if imp.util.find_spec('numpy') else True)
    deps_missing = deps_missing or (False if imp.util.find_spec('pandas') else True)
    deps_missing = deps_missing or (False if imp.util.find_spec('scipy') else True)
    return not deps_missing


try:
    _python_interpreter = bpy.app.binary_path_python
except AttributeError:
    _python_interpreter = sys.executable

def ensure_pip(subprocess_mod):
    pip_found = False
    try:
        print("Check pip module")
        subprocess_mod.check_call([_python_interpreter, "-m", "pip"])
        print("Pip module found!")
        pip_found = True
    except subprocess_mod.CalledProcessError:
        print("Pip module not found")
        
    try:
        if not pip_found:
            print("Running ensurepip module")
            subprocess_mod.check_call([_python_interpreter, "-m", "ensurepip"])
        
        print("Check pip and listing installed packages")
        subprocess_mod.check_call([_python_interpreter, "-m", "pip", "list"])
    except subprocess_mod.CalledProcessError:
        return False
    
    return True
    

def install():
    import subprocess
    
    deps = [
        ('numpy','>=1.21.0'),
        ("pandas", "==1.2.5" if bpy.app.version < (2,9,0) else ""),
        ("scipy", ""),
        ("requests", ""),
        ]

    if not ensure_pip(subprocess):
        raise Chordata_error("Could not execute 'pip', the python package manager. Dependencies could not be installed. Please report this error.")
    
    for package in deps:
        spec = imp.util.find_spec(package[0])
        package = "".join(package)
        if spec:
            print("------ Upgrading with pip: {}".format(package))
            subprocess.check_call([_python_interpreter, "-m", "pip", "install", 
                                                            "--upgrade", package])
        else:
            print("------ Installing with pip: {} in {}".format(package, target_dir))
            subprocess.check_call([_python_interpreter, "-m", "pip", "install", 
                                                            "--target",
                                                            target_dir, package])
    
    print("Reloading armature manager with deps")
    from .ops import armature_manager
    imp.reload(armature_manager)

    print("Reloading AsyncHTTP with deps")
    from .utils import AsyncHTTP
    imp.reload(AsyncHTTP)

    return target_dir
    

class DepsCheckMixin:
    def draw(self, context):
        layout = self.layout
        layout.label(text="The Chordata add-on needs to install some dependencies")
        layout.label(text="The installation might take a few minutes and the UI will freeze in the meantime")
        layout.label(text="You can check the progress at the system console")
    
    def invoke(self, context, event):
        if not check_import():
            return context.window_manager.invoke_props_dialog(self, width=600)

        return self.execute(context)
    
    def check_deps(self):
        print("Chordata addon: checking dependencies..")
        if not check_import():
            try:
                print("Chordata addon: installing dependencies..")
                
                where = install()
                print("Chordata addon: dependencies installed at {}".format(where))
                
            except Exception as e:
                self.report({'ERROR'}, ENGINE_REPORTS["deps_error"].format(e))
                out.error(ENGINE_REPORTS["deps_error"].format(format_exc()))
                return False
        return True