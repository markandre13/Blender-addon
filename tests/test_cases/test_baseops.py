import pytest

# ================================
# =           FIXTURES           =
# ================================

@pytest.fixture
def report_messages(chordata_defaults):
    return chordata_defaults.ENGINE_REPORTS

@pytest.fixture
def base_chordata_operator(chordata_nodetree, chordata_module, mocker):
    base_op = chordata_module.ops.base.BaseChordataNodeOperator()
    base_op.from_tree = chordata_nodetree.name
    testcubenode = chordata_nodetree.nodes.new("TestCubeNodeType")
    base_op.from_node = testcubenode.name
    base_op.report = mocker.Mock()
    base_op._execute = mocker.Mock(return_value=True)
    base_op.start_timer = mocker.Mock(return_value=True)
    base_op.redraw_area_on_rate = mocker.Mock(return_value=True)
    base_op.remove_timer = mocker.Mock(return_value=True)

    return base_op

@pytest.fixture
def base_chordata_operator_running(bpy_test, base_chordata_operator):
    res = base_chordata_operator.execute(bpy_test.context)
    assert res == {'RUNNING_MODAL'}
    assert base_chordata_operator.id_tree.engine_running == True
    yield base_chordata_operator
    try:
        base_chordata_operator.cancel(bpy_test.context)
    except Exception:
        pass

@pytest.fixture
def fake_timer_event():
    from types import SimpleNamespace
    return SimpleNamespace( type= 'TIMER')

# ======  End of FIXTURES  =======


def test_check_engine_running(chordata_nodetree, base_chordata_operator):
    assert chordata_nodetree.engine_running == False
    assert base_chordata_operator.check_engine_running() == False
    chordata_nodetree.engine_running = True
    assert base_chordata_operator.check_engine_running() == True

def test_post_undoredo_reparsing(bpy_test, chordata_module, chordata_nodetree, 
                                    base_chordata_operator_running, mocker):
    base_chordata_operator = base_chordata_operator_running
    #If there's still the node_tree and root node, ask the engine to parse the tree again
    base_chordata_operator.engine.parse_tree = mocker.Mock()
    chordata_module.ops.base.post_undoredo_reparsing(None, base_chordata_operator )
    base_chordata_operator.engine.parse_tree.assert_called_once()

    #If there's something missing avoid parsing
    chordata_nodetree.engine_running = True
    base_chordata_operator.engine.parse_tree = mocker.Mock()
    chordata_nodetree.nodes.remove(chordata_nodetree.nodes[0])
    assert len(chordata_nodetree.nodes) == 0
    chordata_module.ops.base.post_undoredo_reparsing(None, base_chordata_operator )
    base_chordata_operator.engine.parse_tree.assert_not_called()
    assert chordata_nodetree.engine_running == False

def test_undo_redo_handlers_add_remove(bpy_test, mocker, base_chordata_operator):
    res = base_chordata_operator.execute(bpy_test.context)
    assert len(bpy_test.app.handlers.undo_post) == 1
    assert len(bpy_test.app.handlers.redo_post) == 1
    base_chordata_operator.cancel(bpy_test.context)
    assert len(bpy_test.app.handlers.undo_post) == 0
    assert len(bpy_test.app.handlers.redo_post) == 0




def test_baseNodeOp_execution_and_modal(bpy_test, chordata_nodetree, 
                                base_chordata_operator_running, mocker,
                                fake_timer_event):
    
    event = fake_timer_event

    def unset_dirty_flag(arg):
        chordata_nodetree.dirty = False

    base_chordata_operator_running.engine.parse_tree = mocker.Mock(side_effect=unset_dirty_flag)
    base_chordata_operator_running._modal = mocker.Mock()
    base_chordata_operator_running.report = mocker.Mock()

    res = base_chordata_operator_running.modal(bpy_test.context, event)
    assert res == {'PASS_THROUGH'}
    base_chordata_operator_running.engine.parse_tree.assert_not_called()
    base_chordata_operator_running._modal.assert_called_once()
    base_chordata_operator_running.report.assert_not_called()

    # Set dirty flag, engine should parse tree again
    chordata_nodetree.dirty = True
    res = base_chordata_operator_running.modal(bpy_test.context, event)
    base_chordata_operator_running.engine.parse_tree.assert_called_once()
    assert chordata_nodetree.dirty == False

    # Set engine_stop flag, modal should cancel
    chordata_nodetree.engine_stop = True
    base_chordata_operator_running.cancel = mocker.Mock(return_value={'CANCELLED'})
    res = base_chordata_operator_running.modal(bpy_test.context, event)
    assert res == {'CANCELLED'}
    base_chordata_operator_running.cancel.assert_called_once()

    # Brutally remove the nodetree while executing. modal shoudld cancel
    # Here we are simulating this behaviour by renaming the 'from_tree' prop
    chordata_nodetree.engine_stop = False #<--reverting this, was changed some lines above
    base_chordata_operator_running.cancel.reset_mock()
    base_chordata_operator_running.from_tree = "this tree doesn't exists"
    res = base_chordata_operator_running.modal(bpy_test.context, event)
    assert res == {'CANCELLED'}
    base_chordata_operator_running.cancel.assert_called_once()

def test_baseNodeOp_modal_not_TIMER(bpy_test, chordata_defaults, chordata_nodetree, 
                                base_chordata_operator_running, mocker):
    from types import SimpleNamespace
    event = SimpleNamespace( type= 'AKEY')
    res = base_chordata_operator_running.modal(bpy_test.context, event)
    assert res == {'PASS_THROUGH'}
    
    event = SimpleNamespace( type= chordata_defaults.ENGINE_CANCEL_KEYS.copy().pop())
    base_chordata_operator_running.report.reset_mock()
    res = base_chordata_operator_running.modal(bpy_test.context, event)
    assert res == {'CANCELLED'}
    report_msg = chordata_defaults.ENGINE_REPORTS["stopped"]
    base_chordata_operator_running.report.assert_any_call({'INFO'}, report_msg)


def test_cancel(bpy_test, mocker, base_chordata_operator_running, report_messages):
    # if the node_tree gets removed
    base_chordata_operator_running.from_tree = "this tree doesn't exists"
    # the cancel function should report an ERROR
    base_chordata_operator_running.report.reset_mock()
    base_chordata_operator_running.cancel(bpy_test.context)

    base_chordata_operator_running.report.assert_any_call({'ERROR'}, mocker.ANY)

def test_derived_cancel_true(bpy_test, mocker, base_chordata_operator_running, report_messages):
    #if the derived _cancel function return True, 
    #cancel should return the super().cancel() return value
    base_chordata_operator_running._cancel = mocker.Mock(return_value=True)
    ret = base_chordata_operator_running.cancel(bpy_test.context)
    assert ret == {"FINISHED"}

def test_derived_cancel_false(bpy_test, mocker, base_chordata_operator_running, report_messages):
    #if the derived _cancel function return False, cancel should always return {'CANCELLED'}
    base_chordata_operator_running._cancel = mocker.Mock(return_value=False)
    ret = base_chordata_operator_running.cancel(bpy_test.context)
    assert ret == {'CANCELLED'}


def test_baseNodeOp_cancelled_execution(bpy_test, chordata_nodetree, 
                                base_chordata_operator, report_messages, mocker):
    #if the engine is already running, abort execution
    chordata_nodetree.engine_running = True
    res = base_chordata_operator.execute(bpy_test.context)
    assert res == {'CANCELLED'}
    base_chordata_operator.report.assert_called_once_with({'ERROR'}, mocker.ANY)

    #if the _execute function (defined in derived classes) returns False, cancel execution
    base_chordata_operator.report.reset_mock()
    chordata_nodetree.engine_running = False
    base_chordata_operator._execute = mocker.Mock(return_value=False)
    res = base_chordata_operator.execute(bpy_test.context)
    assert res == {'CANCELLED'}
    base_chordata_operator.report.assert_called_once_with({'INFO'}, mocker.ANY)
    

def test_baseNodeOp_modal_catch_keyerror(bpy_test, base_chordata_operator_running, 
                                        fake_timer_event, mocker):
    base_chordata_operator_running._modal = mocker.Mock(side_effect=KeyError)
    base_chordata_operator_running.report = mocker.Mock()
    base_chordata_operator_running.modal(None, fake_timer_event)
    calls = [mocker.call({'ERROR'}, mocker.ANY)]
    base_chordata_operator_running.report.assert_has_calls(calls, any_order=True)


def test_baseNodeOp_modal_catch_exception(bpy_test, base_chordata_operator_running, 
                                        fake_timer_event, mocker):
    base_chordata_operator_running._modal = mocker.Mock(side_effect=Exception)
    base_chordata_operator_running.report = mocker.Mock()
    calls = [mocker.call({'ERROR'}, mocker.ANY)]
    base_chordata_operator_running.modal(None, fake_timer_event)
    base_chordata_operator_running.report.assert_has_calls(calls, any_order=True)

