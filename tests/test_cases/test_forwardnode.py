import pytest
import time

TEST_PORT = 27765

@pytest.fixture
def test_coop_server(copp_server):
	server = copp_server.COPP_Server( port = TEST_PORT )
	server.set_parse_non_copp_bundles(True)
	server.start()
	yield server
	server.close_and_wait()

@pytest.fixture
def forwardnode(chordata_nodetree, engine):
	fdwnode = chordata_nodetree.nodes.new("ForwardNodeType")
	fdwnode.settings.forward_port = TEST_PORT
	
	eng_instance = engine.Engine()
	fwd_rec_node = eng_instance.parse_tree(fdwnode)

	return fwd_rec_node

def test_forwardnode_packet(bpy_test, forwardnode, test_coop_server, copp_ROT_packet, mocker):
	#Send one packet
	forwardnode(copp_ROT_packet)
	time.sleep(0.01)

	count = 0
	assert not test_coop_server.common_packets.empty()

	for pkt in test_coop_server.get():
		count += 1
		for msg in pkt.get(): 
			assert msg in copp_ROT_packet._get_elements()

	assert count == 1

def test_forwardnode_msg(bpy_test, forwardnode, test_coop_server, copp_ROT_packet, mocker):
	#Send one packet
	for msg in copp_ROT_packet.get():
		forwardnode(msg)

	time.sleep(0.01)

	count = 0
	assert not test_coop_server.common_packets.empty()

	copp_ROT_packet.restore()
	for pkt in test_coop_server.get():
		count += 1
		for msg in pkt.get(): 
			assert msg in copp_ROT_packet._get_elements()

	assert count == 1


